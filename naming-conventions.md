## Rosetta Naming Conventions for GitLab Top Level Groups

The Rosetta Top level GitLab groups are allowed only when they follow the correct naming conventions as follows:

- Solution area: **sa**-&lt;**product**&gt;-&lt;group&gt;
- Service delivery: **sd**-&lt;**region**&gt;-&lt;group&gt;
- Software development unit: **sdu**-&lt;**country**&gt;-&lt;group&gt;
- Group Function: **gftl**-&lt;**unit**&gt;-&lt;group&gt;

where:

- **product**: Ericsson product area (bss, oss, pc, cos, nfv)
- **region**: Ericsson market area (mana, mmea, mnea, moai, mela)
- **country**: SDU country (in, cn)
- **group**: User defined group name
- **unit**: Group Function Unit

### Please use the above naming conventions to create your Gitlab Top Level Group
