## Rosetta Naming Conventions for Jfrog Artifactory

The Rosetta Artifactory Repositories are named as follows:

- Repository: &lt;**GitLab group**&gt;-&lt;**repo type**&gt; (**Example:** sa-bss-eoc-docker)

where:

- Group: &lt;Select your GitLab Top Level Group&gt;
- Permission: &lt;GitLab group&gt;
    - associated group is added with all privileges except **Manage**
    - associated users are added (admin users will also have **Manage** privilege)

### Please Submit the Form to Create your Jfrog Artifactory
